import React, {Component} from "react"

class Tugas11 extends Component{
    constructor(props){
        super(props)
        this.state = {
          time : 101,
          showTime : true,
          date : new Date()
        }
        this.flag = true
    }
    tick() {
      this.setState({
        time: this.state.time - 1,
        date: new Date()
      });
    }
    componentDidMount(){
      this.timerID = setInterval(
        () => this.tick(),
        1000
      );
    }
    componentDidUpdate(){
      if(this.state.time===0){
        this.flag = false
      }
    }
    componentWillUnmount(){
      clearInterval(this.timerID)
    }
    render(){
        return(
          <>
            {this.flag && (
              <div style = {{display:'flex'}}>
                <h1 style={{textAlign: 'start', marginRight:'100px'}}>
                  sekarang jam : {this.state.date.toLocaleTimeString("en-US")}
                </h1>
                <h1 style={{textAlign: 'end'}}>
                  hitung mundur: {this.state.time}
                </h1>
              </div>
            )}
          </>
        )
    }
}

export default Tugas11