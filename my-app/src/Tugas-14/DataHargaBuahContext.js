import React, { useState, useEffect, createContext } from "react";
import Axios from 'axios'

export const DataHargaBuahContext = createContext();

export const DataHargaBuahProvider = props => {
  const [dataHargaBuah, setDataHargaBuah] = useState(null);
  useEffect(()=>{
    if(dataHargaBuah===null){
        Axios.get('http://backendexample.sanbercloud.com/api/fruits')
        .then(res=>{
            setDataHargaBuah(res.data)
        })
    }
  },[dataHargaBuah])
  const [inputNama,setInputNama] = useState({nama:'',id:null})
  const [inputHarga,setInputHarga] = useState({harga:'',id:null})
  const [inputBerat,setInputBerat] = useState({berat:'',id:null})

  return (
    <DataHargaBuahContext.Provider value={[
      dataHargaBuah, setDataHargaBuah,inputNama,setInputNama,inputHarga,setInputHarga,
      inputBerat,setInputBerat]}>
      {props.children}
    </DataHargaBuahContext.Provider>
  );
};