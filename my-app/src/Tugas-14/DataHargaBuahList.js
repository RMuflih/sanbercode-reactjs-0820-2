import React, {useContext} from "react"
import {DataHargaBuahContext} from "./DataHargaBuahContext"
import Axios from 'axios'

const DataHargaBuahList = () =>{
    const [dataHargaBuah, setDataHargaBuah,inputNama,setInputNama,inputHarga,setInputHarga,
        inputBerat,setInputBerat] = useContext(DataHargaBuahContext)

    const editForm = (event) =>{
        var idDataBuah = parseInt(event.target.value)
        var dataBuah = dataHargaBuah.find(x=>x.id===idDataBuah)
        setInputNama({...inputNama,id:idDataBuah,nama:dataBuah.name})
        setInputHarga({...inputHarga,id:idDataBuah,harga:dataBuah.price})
        setInputBerat({...inputBerat,id:idDataBuah,berat:dataBuah.weight})
    }

    const deleteForm = (event) =>{
        var idDataBuah=parseInt(event.target.value)
        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
        .then(res=>{
            var newDataHargaBuah = dataHargaBuah.filter(x=>x.id!== idDataBuah)
            setDataHargaBuah(newDataHargaBuah)
        })
    }

    return(
        <div style={{fontFamily:"Times New Roman"}}>
            <h1 style={{marginLeft:"130px"}}>Tabel Harga Buah (Context)</h1>
            <div style={{borderStyle:"groove",width:"600px"}}>
                    <table style={{width:"600px"}}> 
                        <thead>
                            <tr style={{backgroundColor:"#aaa", textAlign:"center"}}>
                                <td><b>Nama</b></td>
                                <td><b>Harga</b></td>
                                <td><b>Berat</b></td>
                                <td><b>Aksi</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            {dataHargaBuah !== null && dataHargaBuah.map((item)=> {
                                return(
                                    <tr key={item.id} style={{backgroundColor:"#ff7f50"}}> 
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight/1000} kg</td>
                                        <td style={{textAlign:'center'}}>
                                            <button onClick={editForm} value={item.id}>Edit</button>&emsp;
                                            <button onClick={deleteForm}value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            <br/>
            <br/>
        </div>
    )

}

export default DataHargaBuahList