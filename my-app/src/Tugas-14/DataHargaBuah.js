import React from "react"
import {DataHargaBuahProvider} from "./DataHargaBuahContext"
import DataHargaBuahList from "./DataHargaBuahList"
import DataHargaBuahForm from "./DataHargaBuahForm"

const DataHargaBuah = () =>{
  return(
    <DataHargaBuahProvider>
      <DataHargaBuahList/>
      <DataHargaBuahForm/>
    </DataHargaBuahProvider>
  )
}

export default DataHargaBuah