import React, {useContext} from 'react'
import {DataHargaBuahContext} from './DataHargaBuahContext'
import Axios from 'axios'

const DataHargaBuahForm = () =>{
    const [dataHargaBuah, setDataHargaBuah,inputNama,setInputNama,inputHarga,setInputHarga,
        inputBerat,setInputBerat] = useContext(DataHargaBuahContext)
    
    const submitForm = (event)=>{
        event.preventDefault()
        if(inputNama.id === null){ 
            Axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{name: inputNama.nama,
            price: parseInt(inputHarga.harga),weight:parseInt(inputBerat.berat)})
            .then(res=>{
                var data=res.data
                setDataHargaBuah([...dataHargaBuah,{id:data.id,name:data.name,price:data.price,weight:data.weight}])
                
                setInputNama({id:null,nama:''})
                setInputHarga({id:null,harga:''})
                setInputBerat({id:null,berat:''})
            })
        } else {
            Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputNama.id}`,{name: inputNama.nama,
            price: parseInt(inputHarga.harga),weight:parseInt(inputBerat.berat)})
            .then(res=>{
                var newDataBuah=dataHargaBuah.map(x=>{
                    if(x.id===inputNama.id){
                        x.name=inputNama.nama
                        x.price=inputHarga.harga
                        x.weight=inputBerat.berat
                    }
                    return x
                })
                setDataHargaBuah(newDataBuah)
                setInputNama({id:null,nama:''})
                setInputHarga({id:null,harga:''})
                setInputBerat({id:null,berat:''})
            })
        }
    }

    const changeFormNama = (event) =>{
        var value = event.target.value
        setInputNama({...inputNama,nama:value})
    }

    const changeFormHarga = (event) =>{
        var value = event.target.value
        setInputHarga({...inputHarga,harga:value})
    }

    const changeFormBerat = (event) =>{
        var value = event.target.value
        setInputBerat({...inputBerat,berat:value})
    }

    return(
        <>
            <form style={{marginLeft:'200px'}} onSubmit={submitForm}>
                <strong style={{marginRight:'33px'}}>Nama</strong>
                <input required type="text" value={inputNama.nama} onChange={changeFormNama}/><br/><br/>
                <strong style={{marginRight:'30px'}}>Harga</strong>
                <input required type="text" value={inputHarga.harga} onChange={changeFormHarga}/><br/><br/>
                <strong style={{marginRight:'36px'}}>Berat</strong>
                <input required type="text" value={inputBerat.berat} onChange={changeFormBerat}/><br/><br/>
                <button style={{marginTop:'20px'}}>Simpan</button>
            </form>
        </>
    )
}

export default DataHargaBuahForm