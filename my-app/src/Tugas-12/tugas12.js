import React from 'react';

class Tugas12 extends React.Component{
    constructor(props){
        super(props)
        this.state={
            dataHargaBuah: [
                {nama:"Semangka", harga:10000, berat:1000},
                {nama:"Anggur", harga:40000, berat:500},
                {nama:"Strawberry", harga:30000, berat:400},
                {nama:"Jeruk", harga:30000, berat:1000},
                {nama:"Mangga", harga:30000, berat:500}
            ],
            inputNama:"",
            inputHarga:'',
            inputBerat:'',
            index:-1
        }
    }

    submitForm = (event)=>{
        event.preventDefault()
        var index = this.state.index
        if(index === -1 && (parseInt(this.state.inputHarga) && parseInt(this.state.inputBerat))!==0){  
            const tambahData = {
                nama : this.state.inputNama,
                harga : parseInt(this.state.inputHarga),
                berat : parseInt(this.state.inputBerat)
            }

            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah,tambahData],
                inputNama:"",
                inputHarga:'',
                inputBerat:''
            })
        } else if (index === -1 || (parseInt(this.state.inputHarga) || parseInt(this.state.inputBerat))===0){
            const tambahData = {
                nama : this.state.inputNama,
                harga : 'Error',
                berat : 'Error'
            }

            this.setState({
                dataHargaBuah: [...this.state.dataHargaBuah,tambahData],
                inputNama:"",
                inputHarga:'',
                inputBerat:''
            })
        } else {
            var newDataHargaBuah = this.state.dataHargaBuah
            newDataHargaBuah[index].nama=this.state.inputNama
            newDataHargaBuah[index].harga=parseInt(this.state.inputHarga)
            newDataHargaBuah[index].berat=parseInt(this.state.inputBerat)

            this.setState({
                dataHargaBuah: [...newDataHargaBuah],
                inputNama:"",
                inputHarga:'',
                inputBerat:'',
                index:-1
            })
        }
    }

    changeFormNama = (event) =>{
        var value = event.target.value
        this.setState({
            inputNama: value,
        })
    }

    changeFormHarga = (event) =>{
        var value = event.target.value
        this.setState({
            inputHarga: value,
        })
    }

    changeFormBerat = (event) =>{
        var value = event.target.value
        this.setState({
            inputBerat: value
        })
    }

    editForm = (event) =>{
        var index = event.target.value
        var dataNamaBuah = this.state.dataHargaBuah[index].nama
        var dataHargaBuah = this.state.dataHargaBuah[index].harga
        var dataBeratBuah = this.state.dataHargaBuah[index].berat
        if((dataHargaBuah && dataBeratBuah)=== 'Error'){
            this.setState({
                inputNama:dataNamaBuah,
                inputHarga:parseInt('0'),
                inputBerat:parseInt('0'),
                index
            })
        } else{
            this.setState({
                inputNama:dataNamaBuah,
                inputHarga:dataHargaBuah,
                inputBerat:dataBeratBuah,
                index
            })
        }
    }

    deleteForm = (event) =>{
        var index=event.target.value
        var newDataHargaBuah = this.state.dataHargaBuah
        newDataHargaBuah.splice(index,1)
        this.setState({
            dataHargaBuah: [...newDataHargaBuah],
            inputNama:"",
            inputHarga:'',
            inputBerat:'',
            index:-1
        })
    }

    render(){
        return(
            <div style={{fontFamily:"Times New Roman"}}>
              <h1 style={{marginLeft:"190px"}}>Tabel Harga Buah</h1>
                <div style={{borderStyle:"groove",width:"600px"}}>
                    <table style={{width:"600px"}}> 
                        <thead>
                            <tr style={{backgroundColor:"#aaa", textAlign:"center"}}>
                                <td><b>Nama</b></td>
                                <td><b>Harga</b></td>
                                <td><b>Berat</b></td>
                                <td><b>Aksi</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            {this.state.dataHargaBuah.map((el,index)=> {
                                return(
                                    <tr style={{backgroundColor:"#ff7f50"}}>
                                        <td>{el.nama}</td>
                                        <td>{el.harga}</td>
                                        <td>{el.berat/1000} kg</td>
                                        <td style={{textAlign:'center'}}>
                                            <button onClick={this.editForm} value={index}>Edit</button>&emsp;
                                            <button onClick={this.deleteForm} value={index}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
                <br/>
                <br/>
                <form style={{marginLeft:'200px'}} onSubmit={this.submitForm}>
                    <strong style={{marginRight:'33px'}}>Nama</strong>
                    <input required type="text" value={this.state.inputNama} onChange={this.changeFormNama}/><br/><br/>
                    <strong style={{marginRight:'30px'}}>Harga</strong>
                    <input required type="text" value={this.state.inputHarga} onChange={this.changeFormHarga}/><br/><br/>
                    <strong style={{marginRight:'36px'}}>Berat</strong>
                    <input required type="text" value={this.state.inputBerat} onChange={this.changeFormBerat}/><br/><br/>
                    <button style={{marginTop:'20px'}}>Simpan</button>
                </form> 
            </div>
        );
    }
}

export default Tugas12