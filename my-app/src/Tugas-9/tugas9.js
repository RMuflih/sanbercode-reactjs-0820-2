import React from 'react'

class Tugas9 extends React.Component{
    render(){
        return(
            <div className="App">
                <div className="container">
                    <h1 className="judul">Form Pembelian Buah</h1>
                    <form action="">
                        <b className="tulisan">Nama Pelanggan</b>
                        <input type="text" name="inputNamaPelanggan" id="inputTulisan"/><br/><br/>
                        <input type="checkbox" name="semangka" id="spasi"/> Semangka<br/>
                        <input type="checkbox" name="jeruk" id="spasi"/> Jeruk<br/>
                        <input type="checkbox" name="nanas" id="spasi"/> Nanas<br/>
                        <input type="checkbox" name="salak" id="spasi"/> Salak<br/>
                        <b className="tulisan">Daftar Item</b><input type="checkbox" name="anggur" id="spasi2"/> Anggur<br/>
                        <button type="submit" className="tombol">Kirim</button>
                    </form>
                </div>
            </div>
        );
    }
}

export default Tugas9