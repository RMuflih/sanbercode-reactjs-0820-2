import React, {useState, useEffect} from 'react'
import Axios from 'axios'

const Tugas13 = ()=>{
    const [dataHargaBuah,setDataHargaBuah] = useState(null)
    const [inputNama,setInputNama] = useState({nama:'',id:null})
    const [inputHarga,setInputHarga] = useState({harga:'',id:null})
    const [inputBerat,setInputBerat] = useState({berat:'',id:null})

    useEffect(()=>{
        if(dataHargaBuah===null){
            Axios.get('http://backendexample.sanbercloud.com/api/fruits')
            .then(res=>{
                setDataHargaBuah(res.data)
            })
        }
    },[dataHargaBuah])

    const submitForm = (event)=>{
        event.preventDefault()
        if(inputNama.id === null){ 
            Axios.post(`http://backendexample.sanbercloud.com/api/fruits`,{name: inputNama.nama,
        price: parseInt(inputHarga.harga),weight:parseInt(inputBerat.berat)})
            .then(res=>{
                var data=res.data
                setDataHargaBuah([...dataHargaBuah,{id:data.id,name:data.name,price:data.price,weight:data.weight}])
                setInputNama({id:null,nama:''})
                setInputHarga({id:null,harga:''})
                setInputBerat({id:null,berat:''})
            })
        } else {
            Axios.put(`http://backendexample.sanbercloud.com/api/fruits/${inputNama.id}`,{name: inputNama.nama,
        price: parseInt(inputHarga.harga),weight:parseInt(inputBerat.berat)})
            .then(res=>{
                var newDataBuah=dataHargaBuah.map(x=>{
                    if(x.id===inputNama.id){
                        x.name=inputNama.nama
                        x.price=inputHarga.harga
                        x.weight=inputBerat.berat
                    }
                    return x
                })
                setDataHargaBuah(newDataBuah)
                setInputNama({id:null,nama:''})
                setInputHarga({id:null,harga:''})
                setInputBerat({id:null,berat:''})
            })
        }
    }

    const changeFormNama = (event) =>{
        var value = event.target.value
        setInputNama({...inputNama,nama:value})
    }

    const changeFormHarga = (event) =>{
        var value = event.target.value
        setInputHarga({...inputHarga,harga:value})
    }

    const changeFormBerat = (event) =>{
        var value = event.target.value
        setInputBerat({...inputBerat,berat:value})
    }

    const editForm = (event) =>{
        var idDataBuah = parseInt(event.target.value)
        var dataBuah = dataHargaBuah.find(x=>x.id===idDataBuah)
        setInputNama({id:idDataBuah,nama:dataBuah.name})
        setInputHarga({id:idDataBuah,harga:dataBuah.price})
        setInputBerat({id:idDataBuah,berat:dataBuah.weight})
    }

    const deleteForm = (event) =>{
        var idDataBuah=parseInt(event.target.value)
        Axios.delete(`http://backendexample.sanbercloud.com/api/fruits/${idDataBuah}`)
        .then(res=>{
            var newDataHargaBuah = dataHargaBuah.filter(x=>x.id!== idDataBuah)
            setDataHargaBuah(newDataHargaBuah)
        })
    }

    return(
        <div style={{fontFamily:"Times New Roman"}}>
            <h1 style={{marginLeft:"130px"}}>Tabel Harga Buah (Hooks)</h1>
                <div style={{borderStyle:"groove",width:"600px"}}>
                    <table style={{width:"600px"}}> 
                        <thead>
                            <tr style={{backgroundColor:"#aaa", textAlign:"center"}}>
                                <td><b>Nama</b></td>
                                <td><b>Harga</b></td>
                                <td><b>Berat</b></td>
                                <td><b>Aksi</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            {dataHargaBuah !== null && dataHargaBuah.map((item)=> {
                                return(
                                    <tr key={item.id} style={{backgroundColor:"#ff7f50"}}> 
                                        <td>{item.name}</td>
                                        <td>{item.price}</td>
                                        <td>{item.weight/1000} kg</td>
                                        <td style={{textAlign:'center'}}>
                                            <button onClick={editForm} value={item.id}>Edit</button>&emsp;
                                            <button onClick={deleteForm} value={item.id}>Delete</button>
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                    </table>
                </div>
            <br/>
            <br/>
            <form style={{marginLeft:'200px'}} onSubmit={submitForm}>
                <strong style={{marginRight:'33px'}}>Nama</strong>
                <input required type="text" value={inputNama.nama} onChange={changeFormNama}/><br/><br/>
                <strong style={{marginRight:'30px'}}>Harga</strong>
                <input required type="text" value={inputHarga.harga} onChange={changeFormHarga}/><br/><br/>
                <strong style={{marginRight:'36px'}}>Berat</strong>
                <input required type="text" value={inputBerat.berat} onChange={changeFormBerat}/><br/><br/>
                <button style={{marginTop:'20px'}}>Simpan</button>
            </form> 
        </div>
    )
}

export default Tugas13