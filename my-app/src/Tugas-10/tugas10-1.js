import React from 'react';
import Tugas102 from './tugas10-2';

class Tugas101 extends React.Component{
    render(){
        let dataHargaBuah = [
            {nama:"Semangka", harga:10000, berat:1000},
            {nama:"Anggur", harga:40000, berat:500},
            {nama:"Strawberry", harga:30000, berat:400},
            {nama:"Jeruk", harga:30000, berat:1000},
            {nama:"Mangga", harga:30000, berat:500}
        ]
        return(
            <div style={{fontFamily:"Times New Roman"}}>
              <h1 style={{marginLeft:"190px"}}>Tabel Harga Buah</h1>
                <div style={{borderStyle:"groove",width:"600px"}}>
                    <table style={{width:"600px"}}> 
                        <thead>
                            <tr style={{backgroundColor:"#aaa", textAlign:"center"}}>
                                <td><b>Nama</b></td>
                                <td><b>Harga</b></td>
                                <td><b>Berat</b></td>
                            </tr>
                        </thead>
                        <tbody>
                            {dataHargaBuah.map(el=> <Tugas102 item={el}/>)}
                        </tbody>
                    </table>    
                </div>
            </div>
        );
    }
}

export default Tugas101