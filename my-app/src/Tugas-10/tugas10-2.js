import React from 'react'

class Tugas102 extends React.Component{
    render(){
        return(
            <>
                <tr style={{backgroundColor:"#ff7f50"}}>
                    <td>{this.props.item.nama}</td>
                    <td>{this.props.item.harga}</td>
                    <td>{this.props.item.berat/1000} kg</td>
                </tr>
            </>
        );
    }
}

export default Tugas102