import React from 'react'
import '../App.css'

const Theme =()=>{
    var flag=0
    const ChangeTheme=() =>{
        var i=0
        var color = document.getElementsByClassName("topnav")
        var nav = document.getElementsByClassName("link")
        if(flag===0){
            color[0].style.backgroundColor='black';
            for(i=0;i<nav.length;i++){nav[i].style.color='white'}
            flag=1
        }
        else if(flag===1){
            color[0].style.backgroundColor='#ebe9da'
            for( i=0;i<nav.length;i++){nav[i].style.color='#5c5a4c'}
            flag=0
        }
    }

    return(
        <>
            <button onClick={ChangeTheme} className={'change-theme'}>Change Theme</button>
        </>
    )
}

export default Theme