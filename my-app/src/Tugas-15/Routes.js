import React from "react";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";
import Tugas9 from '../Tugas-9/tugas9';
import Tugas101 from '../Tugas-10/tugas10-1';
import Tugas11 from '../Tugas-11/tugas11';
import Tugas12 from '../Tugas-12/tugas12'
import Tugas13 from '../Tugas-13/tugas13'
import DataHargaBuah from '../Tugas-14/DataHargaBuah'
import Theme from './tugas15'
import '../App.css';

export default function App() {
    return (
        <Router>
            <div>
                <nav>
                    <ul className={'topnav'}>
                        <li>
                            <Link to="/" className={'link'}>Tugas-9</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-10" className={'link'}>Tugas-10</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-11" className={'link'}>Tugas-11</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-12" className={'link'}>Tugas-12</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-13" className={'link'}>Tugas-13</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-14" className={'link'}>Tugas-14</Link>
                        </li>
                        <li>
                            <Link to="/Tugas-15" className={'link'}>Tugas-15</Link>
                        </li>
                    </ul>
                </nav>
                <Switch>
                    <div style={{margin:'5% 0 0 25%'}}>
                        <Route exact path="/" component={Tugas9}/>
                        <Route path="/Tugas-10">
                            <Tugas101/>
                        </Route>
                        <Route path="/Tugas-11">
                            <Tugas11/>
                        </Route>
                        <Route path="/Tugas-12">
                            <Tugas12/>
                        </Route>
                        <Route path="/Tugas-13">
                            <Tugas13/>
                        </Route>
                        <Route path="/Tugas-14">
                            <DataHargaBuah/>
                        </Route>
                        <Route path="/Tugas-15">
                            <Theme/>
                        </Route>
                    </div>
                </Switch>
            </div>
        </Router>
    );
}